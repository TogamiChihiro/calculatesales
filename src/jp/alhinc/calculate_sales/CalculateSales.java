package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// ！商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// ！商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String TOTAL_OVER_DIGIT = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// ！商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// ！商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "[0-9]{3}","支店" )) {
			return;
		}

		// !商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,"^[0-9a-zA-Z]{8}$","商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File>rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length ; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		for(int i =0; i < rcdFiles.size() ; i++) {
			BufferedReader br = null;
			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;

				//Stringを格納するリストを用意する
				List<String> list = new ArrayList<>();

				while((line = br.readLine()) != null) {
					//リストに要素を追加する
					list.add(line);
				}

				if(list.size() != 3) {
					System.out.println(rcdFiles.get(i).getName()+"のフォーマットが不正です");
					return;
				}

				if (!branchNames.containsKey(list.get(0))) {
					System.out.println(rcdFiles.get(i).getName()+"の支店コードが不正です");
					return;
				}

				if (!commodityNames.containsKey(list.get(1))) {
					System.out.println(rcdFiles.get(i+1).getName()+"の商品コードが不正です");
					return;
				}

				//集計を始めていく
				if(!list.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(list.get(2));
				Long saleAmount = branchSales.get(list.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(list.get(1)) + fileSale;

				if(saleAmount >= 10000000000L){
					System.out.println(TOTAL_OVER_DIGIT);
					return;
				}
				if(commodityAmount >= 10000000000L){
					System.out.println(TOTAL_OVER_DIGIT);
					return;
				}

				branchSales.put(list.get(0), saleAmount);
				commoditySales.put(list.get(1), commodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales, "[0-9]{3}","支店")) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}$","商品")) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param コードと名を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String sort, String errerMsg ) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if(!file.exists()) {
				System.out.println(errerMsg+FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				if((items.length != 2) || (!items[0].matches(sort))){
					System.out.println(errerMsg+FILE_INVALID_FORMAT);
					return false ;
				}
				names.put(items[0],items[1] );
				sales.put(items[0],0L );
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String sort, String errerMsg ) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		File file = new File(path,fileName);
		BufferedWriter bw = null;

		try{
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : names.keySet()) {
				bw.write(key+","+ names.get(key)+","+ sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}